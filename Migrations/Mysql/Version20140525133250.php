<?php
namespace TYPO3\Flow\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
	Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20140525133250 extends AbstractMigration {

	/**
	 * @param Schema $schema
	 * @return void
	 */
	public function up(Schema $schema) {
		// this up() migration is autogenerated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
		
		$this->addSql("ALTER TABLE pipeu_factura_domain_abstracts_abstractdocument ADD primarypersonname VARCHAR(40) DEFAULT NULL, ADD secondarypersonname VARCHAR(40) DEFAULT NULL, CHANGE serialnumber serialnumber BIGINT AUTO_INCREMENT");
		$this->addSql("ALTER TABLE pipeu_factura_domain_abstracts_abstractdocument ADD CONSTRAINT FK_3A5A66AD1702BC1D FOREIGN KEY (primarypersonname) REFERENCES typo3_party_domain_model_personname (persistence_object_identifier) ON DELETE SET NULL");
		$this->addSql("ALTER TABLE pipeu_factura_domain_abstracts_abstractdocument ADD CONSTRAINT FK_3A5A66AD42BD4483 FOREIGN KEY (secondarypersonname) REFERENCES typo3_party_domain_model_personname (persistence_object_identifier) ON DELETE SET NULL");
		$this->addSql("CREATE INDEX IDX_3A5A66AD1702BC1D ON pipeu_factura_domain_abstracts_abstractdocument (primarypersonname)");
		$this->addSql("CREATE INDEX IDX_3A5A66AD42BD4483 ON pipeu_factura_domain_abstracts_abstractdocument (secondarypersonname)");
		$this->addSql("ALTER TABLE pipeu_geo_domain_model_abstracts_abstractpostal CHANGE address address LONGTEXT NOT NULL");
	}

	/**
	 * @param Schema $schema
	 * @return void
	 */
	public function down(Schema $schema) {
		// this down() migration is autogenerated, please modify it to your needs
		$this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
		
		$this->addSql("ALTER TABLE pipeu_factura_domain_abstracts_abstractdocument DROP FOREIGN KEY FK_3A5A66AD1702BC1D");
		$this->addSql("ALTER TABLE pipeu_factura_domain_abstracts_abstractdocument DROP FOREIGN KEY FK_3A5A66AD42BD4483");
		$this->addSql("DROP INDEX IDX_3A5A66AD1702BC1D ON pipeu_factura_domain_abstracts_abstractdocument");
		$this->addSql("DROP INDEX IDX_3A5A66AD42BD4483 ON pipeu_factura_domain_abstracts_abstractdocument");
		$this->addSql("ALTER TABLE pipeu_factura_domain_abstracts_abstractdocument DROP primarypersonname, DROP secondarypersonname, CHANGE serialnumber serialnumber BIGINT AUTO_INCREMENT NOT NULL");
		$this->addSql("ALTER TABLE pipeu_geo_domain_model_abstracts_abstractpostal CHANGE address address LONGTEXT DEFAULT NULL");
	}
}