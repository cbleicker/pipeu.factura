<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Service\Converter;

/**
 * Class AbstractDocumentConverter
 *
 * @package PIPEU\Factura\Service\Converter
 */
abstract class AbstractDocumentConverter implements InterfaceDocumentConverter {

	const PRIORITY = 0, TARGET_TYPE = NULL;

	/**
	 * @return integer
	 */
	public function getPriority() {
		return static::PRIORITY;
	}

	/**
	 * @param $targetType
	 *
	 * @return boolean
	 */
	public function canConvertToTargetType($targetType) {
		return $targetType === static::TARGET_TYPE;
	}
}
