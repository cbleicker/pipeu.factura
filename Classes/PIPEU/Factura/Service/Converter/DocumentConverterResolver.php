<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Service\Converter;

use PIPEU\Factura\Domain\Interfaces\InterfaceDocument;
use PIPEU\Factura\Service\Converter\Exceptions;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class DocumentConverterResolver
 *
 * @package PIPEU\Factura\Service\Converter
 */
class DocumentConverterResolver {

	const INTERFACE_NAME = 'PIPEU\Factura\Service\Converter\InterfaceDocumentConverter';

	/**
	 * @var ReflectionService
	 * @Flow\Inject
	 */
	protected $reflectionService;

	/**
	 * @param string $targetType
	 *
	 * @return InterfaceDocumentConverter
	 * @throws Exceptions\NoConverterFoundException
	 */
	public function resolve($targetType) {
		$possibleDocumentConverter = new \ArrayObject($this->reflectionService->getAllImplementationClassNamesForInterface(self::INTERFACE_NAME));
		$converterIterator = $possibleDocumentConverter->getIterator();
		$resolvedConverter = NULL;

		while ($converterIterator->valid()) {
			$className = $converterIterator->current();

			/** @var InterfaceDocumentConverter $converter */
			$converter = new $className();

			if (!$converter->canConvertToTargetType($targetType)) {
				$converterIterator->next();
				continue;
			}

			if ($resolvedConverter instanceof InterfaceDocumentConverter) {
				if ($resolvedConverter->getPriority() < $converter->getPriority()) {
					$resolvedConverter = $converter;
				}
			} else {
				$resolvedConverter = $converter;
			}

			$converterIterator->next();
		}

		if ($resolvedConverter instanceof InterfaceDocumentConverter) {
			return $resolvedConverter;
		} else {
			throw new Exceptions\NoConverterFoundException('Could not find any converter wich is able to convert to ' . $targetType, 1400595208);
		}
	}
}
