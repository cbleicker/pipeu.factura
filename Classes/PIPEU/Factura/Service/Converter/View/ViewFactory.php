<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Service\Converter\View;

use TYPO3\Flow\Object\ObjectManagerInterface;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class ViewFactory
 *
 * @package PIPEU\Factura\Service\Converter\View
 */
class ViewFactory {

	/*
	 * @var string
	 */
	protected $templatePath = 'resource://@package/Private/Documents/@document.xml';

	/*
	 * @var string
	 */
	protected $layoutRootPath = 'resource://@package/Private/Layouts/';

	/*
	 * @var string
	 */
	protected $partialRootPath = 'resource://@package/Private/Partials/';

	/**
	 * @var string
	 */
	protected $document = 'Standard';

	/**
	 * @var string
	 */
	protected $package = NULL;

	/**
	 * The view
	 *
	 * @var \TYPO3\Fluid\View\StandaloneView
	 * @Flow\Inject
	 */
	protected $view;

	/**
	 * @var ObjectManagerInterface
	 * @Flow\Inject
	 */
	protected $objectManager;

	/**
	 * @param string $document
	 *
	 * @return \TYPO3\Fluid\View\StandaloneView
	 */
	public function create($document) {

		$parts = explode(':', $document);
		if (count($parts) > 1) {
			$this->package = $parts[0];
			$this->document = $parts[1];
		} else {
			$this->document = $document;
		}

		if ($this->package === NULL) {
			$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
			$class = $trace[0]['class'];
			preg_match('/([A-Za-z]*)\\\\([A-Za-z]*)/', $class, $match);
			$this->package = $match[1] . '.' . $match[2];
		}

		$replacements = array(
			'@package' => $this->package,
			'@document' => $this->document
		);

		$view = $this->objectManager->get('\TYPO3\Fluid\View\StandaloneView');

		$template = str_replace(array_keys($replacements), array_values($replacements), $this->templatePath);
		$view->setTemplatePathAndFilename($template);
		$this->view->setTemplatePathAndFilename($template);


		$layoutRootPath = str_replace(array_keys($replacements), array_values($replacements), $this->layoutRootPath);
		$view->setLayoutRootPath($layoutRootPath);
		$this->view->setLayoutRootPath($layoutRootPath);

		$partialRootPath = str_replace(array_keys($replacements), array_values($replacements), $this->partialRootPath);
		$view->setPartialRootPath($partialRootPath);
		$this->view->setPartialRootPath($partialRootPath);

		$this->view->setFormat('xml');
		$view->setFormat('xml');

		$view->getRequest()->setControllerPackageKey($this->package);
		$this->view->getRequest()->setControllerPackageKey($this->package);

		return $view;
		//return $this->view;
	}
}
