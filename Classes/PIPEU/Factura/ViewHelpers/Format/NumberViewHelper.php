<?php
namespace PIPEU\Factura\ViewHelpers\Format;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "TYPO3.Fluid".           *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\I18n\Cldr\Reader\NumbersReader;
use TYPO3\Flow\I18n\Exception as I18nException;
use TYPO3\Flow\I18n\Locale;
use TYPO3\Fluid\Core\ViewHelper\Exception as ViewHelperException;
use TYPO3\Fluid\ViewHelpers\Format\NumberViewHelper as NumberViewHelperOrigin;
use TYPO3\Flow\I18n\Service as LocalizationService;

/**
 * Formats a number with custom precision, decimal point and grouped thousands.
 *
 * @see http://www.php.net/manual/en/function.number-format.php
 */
class NumberViewHelper extends NumberViewHelperOrigin {

	/**
	 * @var LocalizationService
	 * @Flow\Inject
	 */
	protected $localizationService;

	/**
	 * @return string
	 */
	public function renderChildren() {
		$children = (string)parent::renderChildren();
		return $children;
	}

	/**
	 * @param integer $decimals
	 * @param string $decimalSeparator
	 * @param string $thousandsSeparator
	 * @param string $localeFormatLength
	 * @return string
	 */
	public function render($decimals = 2, $decimalSeparator = '.', $thousandsSeparator = ',', $localeFormatLength = NumbersReader::FORMAT_LENGTH_DEFAULT) {
		if (!$this->hasArgument('forceLocale')) {
			$locale = $this->localizationService->getConfiguration()->getCurrentLocale();
			if ($locale instanceof Locale) {
				$this->arguments['forceLocale'] = (string)$locale;
			}
		}
		return parent::render($decimals, $decimalSeparator, $thousandsSeparator, $localeFormatLength);
	}
}
