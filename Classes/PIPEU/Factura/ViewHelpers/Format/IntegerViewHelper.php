<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\ViewHelpers\Format;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class IntegerViewHelper
 *
 * @package PIPEU\Factura\ViewHelpers\Format
 */
class IntegerViewHelper extends AbstractViewHelper{

	/**
	 * @param mixed $value
	 * @return integer
	 */
	public function render($value = NULL) {
		if($value === NULL){
			$value = $this->renderChildren();
		}
		return (integer)(string)$value;
	}

}
