<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\ViewHelpers\Format;

use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\Fluid\ViewHelpers\Format\StripTagsViewHelper;

/**
 * Class PlainTextViewHelper
 *
 * @package PIPEU\Factura\ViewHelpers\Format
 */
class PlainTextViewHelper extends AbstractViewHelper {

	/**
	 * @param string $value
	 *
	 * @return string
	 */
	public function render($value = NULL) {
		if ($value === NULL) {
			$value = $this->renderChildren();
		}
		$stripTagsViewHelper = new StripTagsViewHelper();
		$value = $stripTagsViewHelper->render($value);
		$trimViewHelper = new TrimViewHelper();
		$value = $trimViewHelper->render($value);
		$value = str_ireplace("\t","",$value);
		return $value;
	}
}
