<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\ViewHelpers\Format;

use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class Nl2brViewHelper
 *
 * @package PIPEU\Factura\ViewHelpers\Format
 */
class Nl2brViewHelper extends AbstractViewHelper {

	/**
	 * @param boolean $trim
	 *
	 * @return string
	 */
	public function render($trim = TRUE) {
		$content = $this->renderChildren();
		if ($trim === TRUE) {
			$content = trim($content);
		}
		return nl2br($content);
	}
}
