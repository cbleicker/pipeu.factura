<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\ViewHelpers\Format;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\I18n\Exception as I18nException;
use TYPO3\Flow\I18n\Locale;
use TYPO3\Fluid\Core\ViewHelper\Exception as ViewHelperException;
use TYPO3\Fluid\Core\ViewHelper\Exception\InvalidVariableException;
use TYPO3\Flow\I18n\Service as LocalizationService;

/**
 * Class CurrencyViewHelper
 *
 * @package PIPEU\Factura\ViewHelpers\Format
 */
class CurrencyViewHelper extends \TYPO3\Fluid\ViewHelpers\Format\CurrencyViewHelper {

	/**
	 * @var LocalizationService
	 * @Flow\Inject
	 */
	protected $localizationService;

	/**
	 * @return string
	 */
	public function renderChildren() {
		$children = (string)parent::renderChildren();
		return $children;
	}

	/**
	 * @param string $currencySign
	 * @param string $decimalSeparator
	 * @param string $thousandsSeparator
	 * @return string
	 */
	public function render($currencySign = '', $decimalSeparator = ',', $thousandsSeparator = '.') {
		if ($currencySign === '' || $this->hasArgument('forceLocale')) {
			return parent::render($currencySign, $decimalSeparator, $thousandsSeparator);
		} else {
			$locale = $this->localizationService->getConfiguration()->getCurrentLocale();
			if ($locale instanceof Locale) {
				$this->arguments['forceLocale'] = (string)$locale;
			}
			return parent::render($currencySign, $decimalSeparator, $thousandsSeparator);
		}
	}

}
