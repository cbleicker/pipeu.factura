<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\ViewHelpers;

use TYPO3\Flow\Configuration\ConfigurationManager;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class ConfigurationViewHelper
 *
 * @package PIPEU\Factura\ViewHelpers
 */
class ConfigurationViewHelper extends AbstractViewHelper {

	/**
	 * @var ConfigurationManager
	 * @Flow\Inject
	 */
	protected $configurationManager;

	/**
	 * @param string $configurationPath
	 * @param string $configurationType
	 *
	 * @return string|array
	 */
	public function render($configurationPath, $configurationType = ConfigurationManager::CONFIGURATION_TYPE_SETTINGS) {
		return $this->configurationManager->getConfiguration($configurationType, $configurationPath);
	}
}
