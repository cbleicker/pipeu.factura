<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\ViewHelpers\Document;

use TYPO3\Flow\Annotations as Flow;
use PIPEU\Factura\Domain\Interfaces\InterfaceDocument;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Resource\Resource as FileResource;

/**
 * Class ResourceResolverViewHelper
 *
 * @package PIPEU\Factura\ViewHelpers\Document
 */
class ResourceResolverViewHelper extends AbstractViewHelper {

	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 */
	protected $persistenceManager;

	/**
	 * @var ReflectionService
	 * @Flow\Inject
	 */
	protected $reflectionService;

	/**
	 * @param InterfaceDocument $document
	 * @param string $type
	 * @param string $as
	 *
	 * @return string
	 */
	public function render(InterfaceDocument $document, $type = "pdf", $as = 'resource') {
		$resource = $this->findResource($document, $type);
		$this->templateVariableContainer->add($as, $resource);
		$output = $this->renderChildren();
		$this->templateVariableContainer->remove($as);
		return $output;
	}

	/**
	 * @param InterfaceDocument $document
	 * @param string $type
	 *
	 * @return FileResource
	 */
	protected function findResource(InterfaceDocument $document, $type = "pdf") {
		$resourceType = 'TYPO3\Flow\Resource\Resource';
		$documentType = $this->resolveUnqualifiedClassName($document);
		$documentIdentity = $this->persistenceManager->getIdentifierByObject($document);
		$fileName = $documentType . '.' . $documentIdentity . '.' . $type;
		$query = $this->persistenceManager->createQueryForType($resourceType);
		$query->matching(
			$query->equals('filename', $fileName)
		);
		return $query->execute()->getFirst();
	}

	/**
	 * @param InterfaceDocument $source
	 *
	 * @return string
	 */
	protected function resolveUnqualifiedClassName(InterfaceDocument $source) {
		$className = $this->reflectionService->getClassNameByObject($source);
		$classNamePartitions = Arrays::trimExplode('\\', $className);
		return (string)array_pop($classNamePartitions);
	}
}
