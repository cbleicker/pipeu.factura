<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\ViewHelpers;

use TYPO3\Flow\Exception;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class ClassNameViewHelper
 *
 * @package PIPEU\Factura\ViewHelpers
 */
class ClassNameViewHelper extends AbstractViewHelper {

	/**
	 * @var ReflectionService
	 * @Flow\Inject
	 */
	protected $reflectionService;

	/**
	 * @param mixed $var
	 * @param boolean $unqualified
	 *
	 * @throws \TYPO3\Flow\Exception
	 * @return string
	 */
	public function render($var = NULL, $unqualified = FALSE) {

		if ($var === NULL) {
			$var = $this->renderChildren();
		}

		if (!is_object($var)) {
			throw new Exception('$var must be an "object" but "' . gettype($var) . '" given');
		}

		$className = $this->reflectionService->getClassNameByObject($var);

		if ($unqualified === TRUE) {
			$classNamePartitions = Arrays::trimExplode('\\', $className);
			return array_pop($classNamePartitions);
		} else {
			return $className;
		}
	}
}
