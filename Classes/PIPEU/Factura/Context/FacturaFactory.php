<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Context;

use PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Object\ObjectManagerInterface;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class FacturaFactory
 *
 * @package PIPEU\Factura\Context
 * @Flow\Scope("singleton")
 */
class FacturaFactory {

	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 * @Flow\Transient
	 */
	protected $persistenceManager;

	/**
	 * @var ObjectManagerInterface
	 * @Flow\Inject
	 * @Flow\Transient
	 */
	protected $objectManager;

	/**
	 * @param array $settings
	 * @return void
	 */
	public function injectSettings(array $settings = array()) {
		$this->settings = $settings;
	}

	/**
	 * @param string $country
	 * @return InterfaceFactura
	 */
	public function create($country) {
		/** @var AbstractCountry $country */
		$country = $this->persistenceManager->getObjectByIdentifier($country, 'PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry');
		$factura = new Factura();
		return $factura->setCountry($country);
	}
}
