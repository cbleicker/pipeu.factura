<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Context;

use TYPO3\Flow\Annotations as Flow;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry;

/**
 * Class Factura
 *
 * @package PIPEU\Factura\Context
 * @Flow\Scope("singleton")
 */
class Factura implements InterfaceFactura {

	/**
	 * @var AbstractCountry
	 */
	protected $country;

	/**
	 * @param AbstractCountry $country
	 * @return $this
	 */
	public function setCountry(AbstractCountry $country){
		$this->country = $country;
		return $this;
	}

	/**
	 * @return AbstractCountry
	 */
	public function getCountry() {
		return $this->country;
	}
}
