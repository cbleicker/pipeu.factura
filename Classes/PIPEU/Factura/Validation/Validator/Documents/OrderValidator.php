<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Validation\Validator\Documents;

use PIPEU\Factura\Basket\Session\Storage;
use PIPEU\Factura\NodeTypes\Validation\Validator\MultiLineValidator;
use PIPEU\Factura\Validation\Validator\NonViesVatValidator;
use PIPEU\Factura\Validation\Validator\ViesVatValidator;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Flow\Validation\Error;
use TYPO3\Flow\Validation\Validator\AbstractValidator;
use TYPO3\Flow\Validation\Validator\EmailAddressValidator;
use TYPO3\Flow\Validation\Validator\NotEmptyValidator;
use TYPO3\Flow\Validation\Validator\StringLengthValidator;
use TYPO3\Flow\Validation\ValidatorResolver;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Flow\Annotations as Flow;
use PIPEU\Factura\Domain\Model\Documents\Order;
use PIPEU\Factura\Domain\Interfaces\InterfaceDelivery;

/**
 * Class OrderValidator
 *
 * @package PIPEU\Factura\Validation\Validator\Documents
 */
class OrderValidator extends AbstractValidator {

	/**
	 * @var ReflectionService
	 * @Flow\Inject
	 */
	protected $reflectionService;

	/**
	 * @var ValidatorResolver
	 * @Flow\Inject
	 */
	protected $validationResolver;

	/**
	 * @param Order $value
	 * @return void
	 */
	protected function isValid($value) {
		$order = $value;
		$notEmptyValidator = new NotEmptyValidator();
		$emailAddressValidator = new EmailAddressValidator();

		$multilineValidator = new MultiLineValidator([
			'minimum' => 1,
			'maximum' => 2
		]);

		$stringLengthValidator = new StringLengthValidator([
			'minimum' => 0,
			'maximum' => 255
		]);

		$postalCodeLengthValidator = new StringLengthValidator([
			'minimum' => 0,
			'maximum' => 20
		]);

		$iso2 = ObjectAccess::getPropertyPath($order, 'primaryPostal.country.iso2');

		if ($iso2 !== NULL) {
			$vatValidator = new ViesVatValidator(array('countryIso2' => $order->getPrimaryPostal()->getCountry()->getIso2()));
			$nonViesVatValidator = new NonViesVatValidator(array('webservice' => FALSE, 'countryIso2' => $order->getPrimaryPostal()->getCountry()->getIso2()));

			if ($order->getPrimaryPostal()->getCountry()->getEuMember()) {
				$propertyPath = 'vat';
				$this->result->forProperty($propertyPath)->merge($vatValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
			}

			if (!$order->getPrimaryPostal()->getCountry()->getEuMember()) {
				$propertyPath = 'vat';
				$this->result->forProperty($propertyPath)->merge($nonViesVatValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
			}
		}

		$propertyPath = 'primaryCompany';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryPersonName.title';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryPersonName.firstName';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryPersonName.lastName';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryPostal.address';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($multilineValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryPostal.code';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($postalCodeLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryPostal.city';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryPostal.country';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryPostal.county';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'primaryElectronicAddress.identifier';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($emailAddressValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryCompany';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryPersonName.title';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryPersonName.firstName';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryPersonName.lastName';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryPostal.address';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($multilineValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryPostal.code';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($postalCodeLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryPostal.city';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryPostal.country';
		$this->result->forProperty($propertyPath)->merge($notEmptyValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'secondaryPostal.county';
		$this->result->forProperty($propertyPath)->merge($stringLengthValidator->validate(ObjectAccess::getPropertyPath($order, $propertyPath)));

		$propertyPath = 'facturaItems.products';
		$currentProducts = $order->getFacturaItems()->filter(Storage::getNoDeliveryFilter());

		if ($currentProducts->count() === 0) {
			$this->result->forProperty($propertyPath)->addError(new Error('Empty Basket', 1401550240));
		}

		$propertyPath = 'facturaItems.deliveryProvider';
		$possibleDeliveries = $order->getPossibleDeliveries();
		$currentDeliveries = $order->getFacturaItems()->filter(Storage::getDeliveryFilter());

		if ($currentDeliveries->count() === 0) {
			$this->result->forProperty($propertyPath)->addError(new Error('Missing Delivery Provider', 1401550241));
		}

		/** @var InterfaceDelivery $currentDelivery */
		foreach ($currentDeliveries as $currentDelivery) {
			$isValid = FALSE;
			/** @var InterfaceDelivery $possibleDelivery */
			foreach ($possibleDeliveries as $possibleDelivery) {
				if ($possibleDelivery->getType() === $currentDelivery->getType()) {
					$isValid = TRUE;
				}
			}
			if ($isValid === FALSE) {
				$this->result->forProperty($propertyPath)->addError(new Error('Invalid Delivery Provider', 1401550242));
			}
		}
	}
}
