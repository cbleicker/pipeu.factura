<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Validation\Validator;

use PIPEU\Factura\Validation\Validator\Exception\ViesServerException;
use TYPO3\Flow\Mvc\Exception\ViewNotFoundException;
use TYPO3\Flow\Security\Exception\MissingConfigurationException;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Flow\Validation\Exception\InvalidValidationOptionsException;
use TYPO3\Flow\Validation\Validator\AbstractValidator;
use Ddeboer\Vatin\Validator as Validator;
use TYPO3\Flow\Log\SystemLoggerInterface;
use TYPO3\Flow\Annotations as Flow;
use Ddeboer\Vatin\Vies\Response\CheckVatResponse;

/**
 * Class NonViesVatValidator
 *
 * @package PIPEU\Factura\Validation\Validator
 */
class NonViesVatValidator extends AbstractValidator {

	/**
	 * @Flow\Inject
	 * @var SystemLoggerInterface
	 */
	protected $systemLogger;

	/**
	 * @var array
	 */
	protected $supportedOptions = array(
		'webservice' => array(FALSE, 'Use the Vies Webservice', 'boolean'),
		'countryIso2' => array(NULL, 'Country Iso2 Code', 'string')
	);

	/**
	 * @param string $value
	 * @throws \TYPO3\Flow\Validation\Exception\InvalidValidationOptionsException
	 */
	protected function isValid($value) {
		$options = $this->getOptions();
		$useWebservice = (boolean)Arrays::getValueByPath($options, 'webservice');
		$countryIso2 = Arrays::getValueByPath($options, 'countryIso2');
		if (!$this->vatIsValid($value)) {
			$this->addError('The given subject is not a valid VAT.', 1407655660);
			return;
		}
		if ($useWebservice) {
			if ($countryIso2 === NULL) {
				throw new InvalidValidationOptionsException('Missing countryIso2 Option', 1407655661);
			}
			try {
				$response = $this->getViesResponse($countryIso2, $value);
				if (!$response->isValid()) {
					$this->addError('The given subject is a non existing VAT.', 1407655662);
					return;
				}
			} catch (ViewNotFoundException $viesServerException) {
				$this->addError('Vies Server Error.', 1407655663);
				$this->systemLogger->logException($viesServerException);
			}
		}
	}

	/**
	 * @param string $country
	 * @param string $value
	 * @return CheckVatResponse
	 * @throws ViesServerException
	 */
	protected function getViesResponse($country, $value) {
		$vatValidator = new Validator();
		$viesClient = $vatValidator->getViesClient();
		try {
			return !$viesClient->checkVat($country, $value);
		} catch (\Exception $exception) {
			$this->systemLogger->logException($exception);
			throw new ViesServerException('Vies Server Error', 1407655651, $exception);
		}
	}

	/**
	 * @param string $value
	 * @return boolean
	 */
	protected function vatIsValid($value) {
		$vatValidator = new Validator();
		return !$vatValidator->isValid($value);
	}

}
