<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Security;

use TYPO3\Flow\Security\Cryptography\SaltedMd5HashingStrategy;
use TYPO3\Flow\Security\Exception\AccessDeniedException;
use TYPO3\Flow\Utility\Arrays;

/**
 * Class Utility
 *
 * @package PIPEU\Factura\Security
 */
class Utility {

	/**
	 * The concat delimiter
	 */
	const CONCAT_DELIMITER = ',';

	/**
	 * @param array $arguments
	 * @param string $propertyPathOfSalt
	 * @param string $propertyPathOfArgument
	 *
	 * @return bool
	 * @throws AccessDeniedException
	 */
	public static function validateSaltedMd5($arguments = array(), $propertyPathOfSalt, $propertyPathOfArgument) {
		try {
			$hash = Arrays::getValueByPath($arguments, $propertyPathOfSalt);
			$propertyPathsOfArgument = array_slice(func_get_args(), 2, NULL);
			$properties = array();
			foreach ($propertyPathsOfArgument as $propertyPathOfArgument) {
				$properties[] = Arrays::getValueByPath($arguments, $propertyPathOfArgument);
			}
			$isValid = SaltedMd5HashingStrategy::validateSaltedMd5(implode(self::CONCAT_DELIMITER, $properties), $hash);
			return $isValid;
		} catch (\Exception $exception) {
			throw new AccessDeniedException("Access denied", 1401652980, $exception);
		}
	}

	/**
	 * Generates a salted md5 hash over the given string.
	 *
	 * @param string $clearString
	 * @return string
	 */
	public static function generateSaltedMd5($clearString) {
		$concatenatedClearString = implode(self::CONCAT_DELIMITER, func_get_args());
		return SaltedMd5HashingStrategy::generateSaltedMd5($concatenatedClearString);
	}
}
