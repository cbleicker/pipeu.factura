<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Command;

use TYPO3\Flow\Cli\CommandController;
use PIPEU\Factura\Domain\Repository\Documents\DocumentRepository;
use PIPEU\Factura\Service\Document\DocumentServiceInterface;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Flow\Utility\Files;

/**
 * Class DocumentCommandController
 *
 * @package PIPEU\Factura\Command
 */
class DocumentCommandController extends CommandController {

	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 */
	protected $persistenceManager;

	/**
	 * @var DocumentRepository
	 * @Flow\Inject
	 */
	protected $documentRepository;

	/**
	 * @var DocumentServiceInterface
	 * @Flow\Inject
	 */
	protected $documentService;

	/**
	 * @return void
	 */
	public function testCommand() {
		$this->outputLine('Success');
	}

	/**
	 * @param boolean $dryRun
	 * @return void
	 */
	public function pruneCommand($dryRun = TRUE) {
		$documents = $this->documentRepository->findAll();

		while ($documents->valid()) {
			$this->documentService->pruneDocument($documents->current());
			$this->documentService->prunePdf($documents->current());
			$documents->next();
		}

		if ($dryRun === TRUE) {
			$this->persistenceManager->clearState();
		} else {
			$this->persistenceManager->persistAll();
			$this->documentService->resetAutoInkrement();
		}

		$this->outputLine('Finished');
	}

	/**
	 * @param boolean $dryRun
	 * @return void
	 */
	public function pruneLostResourcesCommand($dryRun = TRUE) {
		$files = Files::readDirectoryRecursively(FLOW_PATH_DATA . 'Persistent/Resources/');
		foreach ($files as $file) {
			$filePartitions = Arrays::trimExplode('/', $file, FALSE);
			$resourcePointer = array_pop($filePartitions);
			$query = $this->persistenceManager->createQueryForType('TYPO3\Flow\Resource\Resource');
			$query->matching(
				$query->equals('resourcePointer', $resourcePointer)
			);
			if($dryRun === FALSE && $query->execute()->count() === 0){
				unlink($file);
			}
		}
		$this->outputLine('Finished');
	}
}
