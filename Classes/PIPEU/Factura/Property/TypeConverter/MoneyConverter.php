<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Property\TypeConverter;

use TYPO3\Flow\Annotations as Flow;

/**
 * Class MoneyConverter
 *
 * @package PIPEU\Factura\Property\TypeConverter
 * @Flow\Scope("singleton")
 */
class MoneyConverter extends AbstractDecimalConverter{

	/**
	 * @var string
	 */
	protected $targetType = 'PIPEU\Factura\Domain\Model\Money';

}

