<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Property\TypeConverter;

use PIPEU\Factura\Domain\Abstracts\AbstractDecimal;
use TYPO3\Flow\Property\TypeConverter\AbstractTypeConverter;
use TYPO3\Flow\Property\Exception\TypeConverterException;
use TYPO3\Flow\Error\Error;
use TYPO3\Flow\Property\PropertyMappingConfigurationInterface;

/**
 * Class DecimalConverter
 *
 * @package PIPEU\Site\Property\TypeConverter
 */
abstract class AbstractDecimalConverter extends AbstractTypeConverter {

	/**
	 * @var array<string>
	 */
	protected $sourceTypes = array('string', 'integer', 'float', 'double');

	/**
	 * @var integer
	 */
	protected $priority = 1;

	/**
	 * @param string $source
	 * @param string $targetType
	 * @param array $convertedChildProperties
	 * @param PropertyMappingConfigurationInterface $configuration
	 * @return AbstractDecimal|Error the target type, or an error object if a user-error occurred
	 * @throws TypeConverterException thrown in case a developer error occurred
	 */
	public function convertFrom($source, $targetType, array $convertedChildProperties = array(), PropertyMappingConfigurationInterface $configuration = NULL) {
		try {
			return $this->toInteger((string)$source, $targetType);
		} catch (\Exception $exception) {
			$exception = new TypeConverterException('Could not convert to "' . $targetType . '"', 1397459667, $exception);
			throw $exception;
		}
	}

	/**
	 * @param string $source
	 * @param string $targetType
	 * @return AbstractDecimal
	 */
	protected function toInteger($source, $targetType){
		return new $targetType(((integer)(((float)$source) * $targetType::getDecimalFactor())));
	}
}
