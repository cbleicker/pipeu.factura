<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Property\TypeConverter\Document;

use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument;
use PIPEU\Factura\Domain\Model\Documents\Invoice;
use PIPEU\Geo\Domain\Model\Interfaces\InterfaceCountry;
use PIPEU\Geo\Domain\Model\Interfaces\InterfacePostal;
use TYPO3\Flow\Property\PropertyMappingConfigurationInterface;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Property\PropertyMapper;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Flow\Utility\Arrays;
use TYPO3\Flow\Error\Error;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Property\PropertyMappingConfigurationBuilder;
use TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter;
use TYPO3\Flow\Reflection\ReflectionService;
use TYPO3\Party\Domain\Model\PersonName;

/**
 * Class CreditConverter
 *
 * @package PIPEU\Factura\Property\TypeConverter\Document
 */
class CreditConverter extends AbstractDocumentConverter {

	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 */
	protected $persistenceManager;

	/**
	 * @var ReflectionService
	 * @Flow\Inject
	 */
	protected $reflectionService;

	/**
	 * @var PropertyMappingConfigurationBuilder
	 * @Flow\Inject
	 */
	protected $propertyMappingConfigurationBuilder;

	/**
	 * @var PropertyMapper
	 * @Flow\Inject
	 */
	protected $propertyMapper;

	/**
	 * {@inheritdoc}
	 */
	protected $sourceTypes = array('object');

	/**
	 * {@inheritdoc}
	 */
	protected $targetType = 'PIPEU\Factura\Domain\Model\Documents\Credit';

	/**
	 * {@inheritdoc}
	 */
	public function canConvertFrom($source, $targetType) {
		return $source instanceof InterfaceFacturaDocument;
	}

	/**
	 * Converts a document to credit
	 *
	 * @param InterfaceFacturaDocument $source
	 * @param string $targetType
	 * @param array $convertedChildProperties
	 * @param PropertyMappingConfigurationInterface $configuration
	 *
	 * @return Invoice|Error the target type, or an error object if a user-error occurred
	 */
	public function convertFrom($source, $targetType, array $convertedChildProperties = array(), PropertyMappingConfigurationInterface $configuration = NULL) {

		if (!($configuration instanceof PropertyMappingConfigurationInterface)) {
			$configuration = $this->propertyMappingConfigurationBuilder->build();
		}

		$configuration->allowAllProperties();

		$configuration->forProperty('primaryElectronicAddress')
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE)
			->allowAllProperties();

		$configuration->forProperty('secondaryElectronicAddress')
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE)
			->allowAllProperties();

		$configuration->forProperty('primaryPostal')
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE)
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE)
			->allowAllProperties();

		$configuration->forProperty('secondaryPostal')
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE)
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_OVERRIDE_TARGET_TYPE_ALLOWED, TRUE)
			->allowAllProperties();

		$configuration->forProperty('primaryPersonName')
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE)
			->allowAllProperties();

		$configuration->forProperty('secondaryPersonName')
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE)
			->allowAllProperties();

		$targetProperties = new \ArrayObject();

		Arrays::setValueByPath($targetProperties, 'parentDocument', $this->persistenceManager->getIdentifierByObject($source));

		$this->transferObjectPropertyByPath($source, $targetProperties, 'localeIdentifier');
		$this->transferObjectPropertyByPath($source, $targetProperties, 'vat');

		$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryElectronicAddress.identifier');
		$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryElectronicAddress.usage');
		$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryElectronicAddress.type');
		$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryElectronicAddress.approved');

		$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryElectronicAddress.identifier');
		$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryElectronicAddress.usage');
		$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryElectronicAddress.type');
		$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryElectronicAddress.approved');

		if ($source->getPrimaryPostal() instanceof InterfacePostal) {
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPostal.address');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPostal.city');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPostal.code');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPostal.county');
			Arrays::setValueByPath($targetProperties, 'primaryPostal.__type', $this->reflectionService->getClassNameByObject($source->getPrimaryPostal()));
			if ($source->getPrimaryPostal()->getCountry() instanceof InterfaceCountry) {
				Arrays::setValueByPath($targetProperties, 'primaryPostal.country', $this->persistenceManager->getIdentifierByObject($source->getPrimaryPostal()->getCountry()));
			}
		}

		if ($source->getSecondaryPostal() instanceof InterfacePostal) {
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPostal.address');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPostal.city');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPostal.code');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPostal.county');
			Arrays::setValueByPath($targetProperties, 'secondaryPostal.__type', $this->reflectionService->getClassNameByObject($source->getSecondaryPostal()));
			if ($source->getSecondaryPostal()->getCountry() instanceof InterfaceCountry) {
				Arrays::setValueByPath($targetProperties, 'secondaryPostal.country', $this->persistenceManager->getIdentifierByObject($source->getSecondaryPostal()->getCountry()));
			}
		}

		$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryCompany');
		$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryCompany');

		if ($source->getPrimaryPersonName() instanceof PersonName) {
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPersonName.title');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPersonName.firstName');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPersonName.middleName');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPersonName.lastName');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPersonName.otherName');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'primaryPersonName.alias');
		}

		if ($source->getSecondaryPersonName() instanceof PersonName) {
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPersonName.title');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPersonName.firstName');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPersonName.middleName');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPersonName.lastName');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPersonName.otherName');
			$this->transferObjectPropertyByPath($source, $targetProperties, 'secondaryPersonName.alias');
		}

		/** @var InterfaceFacturaDocument $credit */
		$credit = $this->propertyMapper->convert($targetProperties->getArrayCopy(), $this->targetType, $configuration);

		$facturaItemConfiguration = $this->propertyMappingConfigurationBuilder->build();
		$facturaItemConfiguration
			->setTypeConverterOption('TYPO3\Flow\Property\TypeConverter\PersistentObjectConverter', PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE)
			->allowAllProperties();

		$facturaItems = $source->getFacturaItems();
		$facturaItems->first();
		while ($facturaItems->current()) {

			$facturaItemTargetProperties = new \ArrayObject();

			switch ($this->reflectionService->getClassNameByObject($facturaItems->current())) {
				case 'PIPEU\Factura\Domain\Model\Documents\Items\PhysicalProduct':
					$this->transferObjectPropertyByPath($facturaItems->current(), $facturaItemTargetProperties, 'title');
					$this->transferObjectPropertyByPath($facturaItems->current(), $facturaItemTargetProperties, 'subtitle');
					$this->transferObjectPropertyByPath($facturaItems->current(), $facturaItemTargetProperties, 'width');
					$this->transferObjectPropertyByPath($facturaItems->current(), $facturaItemTargetProperties, 'length');
					$this->transferObjectPropertyByPath($facturaItems->current(), $facturaItemTargetProperties, 'depth');
					Arrays::setValueByPath($facturaItemTargetProperties, 'unitPrice', (string)ObjectAccess::getPropertyPath($facturaItems->current(), 'unitPrice'));
					Arrays::setValueByPath($facturaItemTargetProperties, 'unitWeight', (string)ObjectAccess::getPropertyPath($facturaItems->current(), 'unitWeight'));
					Arrays::setValueByPath($facturaItemTargetProperties, 'tax', (string)ObjectAccess::getPropertyPath($facturaItems->current(), 'tax'));
					Arrays::setValueByPath($facturaItemTargetProperties, 'unit', (string)ObjectAccess::getPropertyPath($facturaItems->current(), 'unit'));
					$facturaItem = $this->propertyMapper->convert($facturaItemTargetProperties->getArrayCopy(), $this->reflectionService->getClassNameByObject($facturaItems->current()), $facturaItemConfiguration);
					$credit->addFacturaItem($facturaItem);
					break;
			}

			$facturaItems->next();
		}

		return $credit;
	}
}
