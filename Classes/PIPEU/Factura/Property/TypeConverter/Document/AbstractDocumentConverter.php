<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Property\TypeConverter\Document;
use TYPO3\Flow\Property\TypeConverter\AbstractTypeConverter;
use TYPO3\Flow\Reflection\Exception\PropertyNotAccessibleException;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Flow\Utility\Arrays;

/**
 * Class AbstractDocumentConverter
 *
 * @package PIPEU\Factura\Property\TypeConverter\Document
 */
abstract class AbstractDocumentConverter extends AbstractTypeConverter {

	/**
	 * @param object $source
	 * @param \ArrayObject $target
	 * @param string $propertyPath
	 * @param boolean $forceDirectAccess
	 *
	 * @throws \InvalidArgumentException
	 */
	protected function transferObjectPropertyByPath($source, \ArrayObject $target, $propertyPath, $forceDirectAccess = FALSE) {
		if (!is_object($source)) {
			throw new \InvalidArgumentException("Only objects supported to extract transfer properties", 1400853328);
		}
		if (!is_string($propertyPath)) {
			throw new \InvalidArgumentException("Propertypath must be of type of string", 1400853329);
		}
		try {
			$propertyValue = ObjectAccess::getPropertyPath($source, $propertyPath, $forceDirectAccess);
			if ($propertyValue !== NULL) {
				Arrays::setValueByPath($target, $propertyPath, $propertyValue);
			}
		} catch (PropertyNotAccessibleException $propertyNotAccessibleException) {
			// Unaccessible properties will result in NULL
		}
	}

}
