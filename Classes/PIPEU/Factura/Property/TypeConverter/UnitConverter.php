<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Property\TypeConverter;

use PIPEU\Factura\Domain\Model\Unit;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class UnitConverter
 *
 * @package PIPEU\Factura\Property\TypeConverter
 * @Flow\Scope("singleton")
 */
class UnitConverter extends AbstractDecimalConverter{

	/**
	 * @var string
	 */
	protected $targetType = 'PIPEU\Factura\Domain\Model\Unit';

	/**
	 * @param string $source
	 * @param string $targetType
	 * @return Unit
	 */
	protected function toInteger($source, $targetType) {
		$integer = (integer)$source;
		if($integer < 0){
			$integer *= -1;
		}
		$source = (string)($integer);
		return parent::toInteger($source, $targetType);
	}
}

