<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\FlowQuery\Operations;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Eel\FlowQuery\Operations\AbstractOperation;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Eel\FlowQuery\FlowQuery;

/**
 * Class SortOperation
 *
 * @package PIPEU\Factura\FlowQuery\Operations
 */
class SortOperation extends AbstractOperation {

	const SORTING_DESC = 'DESC', SORTING_ASC = 'ASC';

	/**
	 * {@inheritdoc}
	 * @var string
	 */
	static protected $shortName = 'sort';

	/**
	 * {@inheritdoc}
	 * @param FlowQuery $flowQuery the FlowQuery object
	 * @param array $arguments the context index to fetch from
	 * @return void
	 */
	public function evaluate(FlowQuery $flowQuery, array $arguments) {

		$context = $flowQuery->getContext();
		if (isset($arguments[0])) {

			$propertyPath = $arguments[0];
			$direction = isset($arguments[1]) ? $arguments[1] : NULL;
			$desc = function ($a, $b) use ($propertyPath) {
				return ObjectAccess::getPropertyPath($a, $propertyPath) < ObjectAccess::getPropertyPath($b, $propertyPath);
			};
			$asc = function ($a, $b) use ($propertyPath) {
				return ObjectAccess::getPropertyPath($a, $propertyPath) > ObjectAccess::getPropertyPath($b, $propertyPath);
			};

			switch ($direction) {
				case self::SORTING_DESC:
					@usort($context, $desc);
					break;
				case self::SORTING_ASC:
					@usort($context, $asc);
					break;
			}

			$flowQuery->setContext($context);
		}
	}
}
