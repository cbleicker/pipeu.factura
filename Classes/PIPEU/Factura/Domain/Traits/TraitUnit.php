<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Traits;

use PIPEU\Factura\Domain\Model\Unit;

/**
 * Class TraitUnit
 *
 * @package PIPEU\Factura\Domain\Traits
 */
trait TraitUnit {

	/**
	 * @return Unit
	 */
	public function getUnit() {
		return $this->unit;
	}

	/**
	 * @param Unit $unit
	 * @return $this
	 */
	public function setUnit(Unit $unit) {
		$this->unit = $unit;
		return $this;
	}
}
