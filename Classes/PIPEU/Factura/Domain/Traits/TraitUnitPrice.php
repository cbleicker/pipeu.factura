<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Traits;

use PIPEU\Factura\Domain\Model\Money;

/**
 * Class TraitUnitPrice
 *
 * @package PIPEU\Factura\Domain\Traits
 */
trait TraitUnitPrice {

	/**
	 * @return Money
	 */
	public function getUnitPrice() {
		return $this->unitPrice;
	}

	/**
	 * @param Money $unitPrice
	 * @return $this
	 */
	public function setUnitPrice(Money $unitPrice) {
		$this->unitPrice = $unitPrice;
		return $this;
	}
}
