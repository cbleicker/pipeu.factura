<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Traits;

use PIPEU\Factura\Domain\Abstracts\AbstractState;
use PIPEU\Factura\Domain\Interfaces\InterfaceDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use TYPO3\Flow\Exception;
use TYPO3\Party\Domain\Model\ElectronicAddress;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractPostal;
use PIPEU\Factura\Context\InterfaceFactura as FacturaContext;
use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use Doctrine\Common\Collections\Collection;
use TYPO3\Party\Domain\Model\PersonName;
use PIPEU\Factura\Security\Utility as SecurityUtility;

/**
 * Class TraitDocument
 *
 * @package PIPEU\Factura\Domain\Traits
 */
trait TraitDocument {

	/**
	 * @param AbstractDocument $parentDocument
	 *
	 * @return $this;
	 */
	public function setParentDocument(AbstractDocument $parentDocument) {
		$this->parentDocument = $parentDocument;
		return $this;
	}

	/**
	 * @return AbstractDocument
	 */
	public function getParentDocument() {
		return $this->parentDocument;
	}

	/**
	 * @return Collection<AbstractDocument>
	 */
	public function getChildDocuments() {
		return $this->childDocuments;
	}

	/**
	 * @param $type
	 *
	 * @return \Closure
	 */
	public static function childTypeFilter($type) {
		return function (InterfaceDocument $document) use ($type) {
			return is_a($document, $type);
		};
	}

	/**
	 * @return AbstractDocument
	 */
	public function getMainDocument() {
		if ($this->getParentDocument() instanceof AbstractDocument) {
			return $this->getParentDocument()->getMainDocument();
		} else {
			return $this;
		}
	}

	/**
	 * @return string
	 */
	public function getLocaleIdentifier() {
		return $this->localeIdentifier;
	}

	/**
	 * @param string $localeIdentifier
	 *
	 * @return $this
	 */
	public function setLocaleIdentifier($localeIdentifier) {
		$this->localeIdentifier = $localeIdentifier;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getSerialNumber() {
		return $this->serialNumber;
	}

	/**
	 * @param \DateTime $dateTime
	 *
	 * @return $this
	 */
	public function setDateTime(\DateTime $dateTime) {
		$this->dateTime = $dateTime;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateTime() {
		return $this->dateTime;
	}

	/**
	 * @return FacturaContext
	 */
	public function getContext() {
		return $this->context;
	}

	/**
	 * @param AbstractPostal $primaryPostal
	 *
	 * @return $this
	 */
	public function setPrimaryPostal(AbstractPostal $primaryPostal) {
		$this->primaryPostal = $primaryPostal;
		return $this;
	}

	/**
	 * @return AbstractPostal
	 */
	public function getPrimaryPostal() {
		return $this->primaryPostal;
	}

	/**
	 * @param AbstractPostal $secondaryPostal
	 *
	 * @return $this
	 */
	public function setSecondaryPostal(AbstractPostal $secondaryPostal) {
		$this->secondaryPostal = $secondaryPostal;
		return $this;
	}

	/**
	 * @return AbstractPostal
	 */
	public function getSecondaryPostal() {
		return $this->secondaryPostal;
	}

	/**
	 * @param ElectronicAddress $primaryElectronicAddress
	 *
	 * @return $this
	 */
	public function setPrimaryElectronicAddress(ElectronicAddress $primaryElectronicAddress) {
		$this->primaryElectronicAddress = $primaryElectronicAddress;
		return $this;
	}

	/**
	 * @return ElectronicAddress
	 */
	public function getPrimaryElectronicAddress() {
		return $this->primaryElectronicAddress;
	}

	/**
	 * @param ElectronicAddress $secondaryElectronicAddress
	 *
	 * @return $this
	 */
	public function setSecondaryElectronicAddress(ElectronicAddress $secondaryElectronicAddress) {
		$this->secondaryElectronicAddress = $secondaryElectronicAddress;
		return $this;
	}

	/**
	 * @return ElectronicAddress
	 */
	public function getSecondaryElectronicAddress() {
		return $this->secondaryElectronicAddress;
	}

	/**
	 * @param AbstractState $state
	 *
	 * @return $this
	 */
	public function addState(AbstractState $state) {
		$this->states->add($state);
		return $this;
	}

	/**
	 * @param AbstractState $state
	 *
	 * @return $this
	 */
	public function removeState(AbstractState $state) {
		$this->states->removeElement($state);
		if ($state === $this->primaryState) {
			$firstState = $this->getStates()->first();
			if ($firstState instanceof InterfaceState) {
				$this->primaryState = $firstState;
			} else {
				$this->primaryState = NULL;
			}
		}
		return $this;
	}

	/**
	 * @param Collection <AbstractState> $states
	 *
	 * @return $this
	 */
	public function setStates(Collection $states) {
		if ($this->primaryState !== NULL && !$states->contains($this->primaryState)) {
			$this->primaryState = $states->first();
		}
		$this->states = $states;
		return $this;
	}

	/**
	 * @return Collection<AbstractState>
	 */
	public function getStates() {
		return clone $this->states;
	}

	/**
	 * @param AbstractState $state
	 *
	 * @return $this
	 */
	public function setPrimaryState(AbstractState $state) {
		$this->primaryState = $state;
		if (!$this->states->contains($state)) {
			$this->states->add($state);
		}
		return $this;
	}

	/**
	 * @return AbstractState
	 */
	public function getPrimaryState() {
		return $this->primaryState;
	}

	/**
	 * @return boolean
	 */
	public function getIsReversed(){
		$reversedFilter = function(InterfaceState $state){
			return $state->getCode() === InterfaceState::CODE_REVERSED;
		};
		$reversedStates = $this->getStates()->filter($reversedFilter);
		return (boolean)$reversedStates->count();
	}

	/**
	 * @param PersonName $secondaryPersonName
	 *
	 * @return $this
	 */
	public function setSecondaryPersonName(PersonName $secondaryPersonName) {
		$this->secondaryPersonName = $secondaryPersonName;
		return $this;
	}

	/**
	 * @return PersonName
	 */
	public function getSecondaryPersonName() {
		return $this->secondaryPersonName;
	}

	/**
	 * @param PersonName $primaryPersonName
	 *
	 * @return $this
	 */
	public function setPrimaryPersonName(PersonName $primaryPersonName) {
		$this->primaryPersonName = $primaryPersonName;
		return $this;
	}

	/**
	 * @return PersonName
	 */
	public function getPrimaryPersonName() {
		return $this->primaryPersonName;
	}

	/**
	 * @param string $primaryCompany
	 * @return $this
	 */
	public function setPrimaryCompany($primaryCompany = NULL) {
		$this->primaryCompany = $primaryCompany;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPrimaryCompany() {
		return $this->primaryCompany;
	}

	/**
	 * @param string $secondaryCompany
	 * @return $this
	 */
	public function setSecondaryCompany($secondaryCompany = NULL) {
		$this->secondaryCompany = $secondaryCompany;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSecondaryCompany() {
		return $this->secondaryCompany;
	}

	/**
	 * @return string
	 */
	public function getIdentityHash() {
		return SecurityUtility::generateSaltedMd5($this->getIdentity());
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getAuthenticationHash() {
		if($this->getPrimaryElectronicAddress() instanceof ElectronicAddress){
			return SecurityUtility::generateSaltedMd5($this->getIdentityHash(), $this->getPrimaryElectronicAddress()->getIdentifier());
		}else{
			throw new Exception("Authentication-Hash generation failed", 1401960025);
		}
	}

	/**
	 * @return string
	 */
	public function getIdentity() {
		return $this->persistenceManager->getIdentifierByObject($this);
	}

	/**
	 * @return string
	 */
	public function getType() {
		return static::TYPE;
	}

	/**
	 * @param boolean $archived
	 * @return $this
	 */
	public function setArchived($archived = TRUE) {
		$this->archived = $archived;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getArchived() {
		return $this->archived;
	}
}
