<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Traits;

use PIPEU\Factura\Domain\Model\Money;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TraitFacturaItem
 *
 * @package PIPEU\Factura\Domain\Traits
 */
trait TraitFacturaItem {

	use TraitUnit, TraitUnitPrice, TraitTax, TraitAvailableFrom;

	/**
	 * @return Money
	 */
	public function getPrice() {
		$value = $this->getUnitPrice()->getValue() * $this->getUnit()->getValue() / $this->getUnit()->getDecimalFactor();
		return new Money((integer)$value);
	}

}
