<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Traits;

/**
 * Class TraitAvailableFrom
 *
 * @package PIPEU\Factura\Domain\Traits
 */
trait TraitAvailableFrom {

	/**
	 * @param \DateTime $availableFrom
	 * @return $this
	 */
	public function setAvailableFrom(\DateTime $availableFrom = NULL) {
		$this->availableFrom = $availableFrom;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getAvailableFrom() {
		return $this->availableFrom;
	}
}
