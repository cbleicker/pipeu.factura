<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Traits;

use PIPEU\Factura\Domain\Model\Weight;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TraitWeight
 *
 * @package PIPEU\Factura\Domain\Traits
 */
trait TraitWeight {

	/**
	 * @return Weight
	 */
	public function getUnitWeight() {
		return $this->unitWeight;
	}

	/**
	 * @param Weight $unitWeight
	 * @return $this
	 */
	public function setUnitWeight(Weight $unitWeight) {
		$this->unitWeight = $unitWeight;
		return $this;
	}
}
