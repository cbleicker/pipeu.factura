<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Traits;

use TYPO3\Flow\Exception;

/**
 * Class TraitDecimal
 *
 * @package PIPEU\Factura\Domain\Traits
 */
trait TraitDecimal {

	/**
	 * @return integer
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return (string)number_format(($this->getValue() / static::getDecimalFactor()), static::getDecimalPoints(), '.', '');
	}

	/**
	 * @return integer
	 */
	public static function getDecimalPoints(){
		throw new Exception('Missing Implementation of getDecimalPoints() method', 1397903490);
	}

	/**
	 * @return integer
	 */
	public static function getDecimalFactor() {
		return (integer)pow(10, static::getDecimalPoints());
	}

	/**
	 * @return string
	 */
	public function jsonSerialize() {
		return (string)$this;
	}
}
