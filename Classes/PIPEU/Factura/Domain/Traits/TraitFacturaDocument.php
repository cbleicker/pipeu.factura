<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Factura\Domain\Abstracts\AbstractState;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaItem;
use PIPEU\Factura\Domain\Model\Documents\Items\Tax;
use PIPEU\Factura\Domain\Model\Tax as DecimalTax;
use PIPEU\Factura\Domain\Model\Money;
use Doctrine\Common\Collections\Collection;
use PIPEU\Factura\Error\Exception\FacturaItemCurrentlyNotAvailableException;
use PIPEU\Factura\Validation\Validator\ViesVatValidator;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractPostal;
use PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry;
use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use Ddeboer\Vatin\Validator as Validator;

/**
 * Class TraitFacturaDocument
 *
 * @package PIPEU\Factura\Domain\Traits
 */
trait TraitFacturaDocument {

	/**
	 * @return Collection<AbstractFacturaItem>
	 */
	public function getFacturaItems() {
		return $this->facturaItems;
	}

	/**
	 * @param AbstractFacturaItem $facturaItem
	 * @return $this
	 * @throws \PIPEU\Factura\Error\Exception\FacturaItemCurrentlyNotAvailableException
	 */
	public function addFacturaItem(AbstractFacturaItem $facturaItem) {
		if($facturaItem->getAvailableFrom() > new \DateTime()){
			throw new FacturaItemCurrentlyNotAvailableException('This factura item is currently not available.', 1410781709);
		}
		if (!$this->getFacturaItems()->contains($facturaItem)) {
			$facturaItem->setDocument($this);
			$this->getFacturaItems()->add($facturaItem);
		}
		return $this;
	}

	/**
	 * @param AbstractFacturaItem $facturaItem
	 * @return $this
	 */
	public function removeFacturaItem(AbstractFacturaItem $facturaItem) {
		if ($this->getFacturaItems()->contains($facturaItem)) {
			$this->getFacturaItems()->removeElement($facturaItem);
		}
		return $this;
	}

	/**
	 * @return $this
	 */
	public function pruneFacturaItems() {
		$this->getFacturaItems()->clear();
		return $this;
	}

	/**
	 * @param Collection $facturaItems
	 * @return $this
	 */
	public function setFacturaItems(Collection $facturaItems) {
		$this->facturaItems = $facturaItems;
		return $this;
	}

	/**
	 * @param string $vat
	 * @return $this
	 */
	public function setVat($vat = NULL) {
		$this->vat = $vat !== NULL && trim($vat) === '' ? NULL : $vat;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVat() {
		return $this->vat;
	}

	/**
	 * @return Money
	 */
	public function getSummary() {
		$summary = 0;
		/** @var AbstractFacturaItem $facturaItem */
		foreach ($facturaItems = $this->getFacturaItems() as $facturaItem) {
			$summary += $facturaItem->getPrice()->getValue();
		}
		return new Money($summary);
	}

	/**
	 * @return Money
	 */
	public function getSummaryGross() {
		$gross = 0;
		$taxIterator = $this->getTaxes()->getIterator();
		while ($taxIterator->valid()) {
			/** @var Tax $tax */
			$tax = $taxIterator->current();
			$gross += $tax->getPrice()->getValue();
			$taxIterator->next();
		}
		$gross += $this->getSummary()->getValue();
		return new Money($gross);
	}

	/**
	 * @return ArrayCollection<Tax>
	 */
	public function getTaxes() {

		$taxes = new ArrayCollection();
		$facturaItems = $this->getFacturaItems();
		/** @var InterfaceFacturaItem $facturaItem */
		foreach ($facturaItems as $facturaItem) {
			if ($this->getIsTaxable() === TRUE) {
				$taxPrice = new Money((integer)($facturaItem->getPrice()->getValue() * $facturaItem->getTax()->getValue() / $facturaItem->getTax()->getDecimalFactor() / $facturaItem->getTax()->getDecimalFactor()));
			} else {
				$taxPrice = new Money(0);
			}
			$currentTax = $taxes->filter($this->filterTaxByTax($facturaItem->getTax()->getValue()))->first();
			if (!$currentTax instanceof Tax) {
				$tax = new Tax($taxPrice, $facturaItem->getTax());
			} else {
				$taxValue = $currentTax->getTax()->getValue(); // + $taxPrice->getValue();
				$newTaxPrice = new Money($currentTax->getPrice()->getValue() + $taxPrice->getValue());
				$tax = new Tax($newTaxPrice, new DecimalTax($taxValue));
				$taxes->removeElement($currentTax);
			}
			$taxes->add($tax);
		}
		return $taxes;
	}

	/**
	 * @return boolean
	 */
	public function getIsTaxable() {

		if ($this->getSecondaryPostal() instanceof AbstractPostal) {
			$country = $this->getSecondaryPostal()->getCountry();
		} elseif ($this->getPrimaryPostal() instanceof AbstractPostal) {
			$country = $this->getPrimaryPostal()->getCountry();
		} else {
			$country = NULL;
		}

		if (!($country instanceof AbstractCountry)) {
			return TRUE;
		}

		/** Country and ShowCountry equal */
		if ($country->getIso2() === $this->getContext()->getCountry()->getIso2()) {
			return TRUE;
		}
		/** Third Community */
		if ($country->getEuMember() === FALSE) {
			return FALSE;
		}
		/** Inner Community B2B */
		if ($country->getEuMember() === TRUE && $this->getVat() !== NULL) {
			if ($this->getPrimaryPostal() instanceof AbstractPostal) {
				/** @var AbstractCountry $primaryCountry */
				$primaryCountry = $this->getPrimaryPostal()->getCountry();
				$viewVatValidator = new ViesVatValidator(array('countryIso2'=>$primaryCountry->getIso2()));
				return $viewVatValidator->validate($this->getVat())->hasErrors();
			}
		}
		return TRUE;
	}

	/**
	 * @param integer $value
	 * @return \Closure
	 */
	protected function filterTaxByTax($value) {
		return function (Tax $existingTax) use ($value) {
			return $existingTax->getTax()->getValue() === $value;
		};
	}

	/**
	 * @return Collection<InterfaceState>
	 */
	public function getPaidStates() {
		return $this->getStates()->filter(AbstractState::filterStateByCode(InterfaceState::CODE_PAID));
	}

	/**
	 * return boolean
	 */
	public function getIsPaid() {
		return (boolean)$this->getPaidStates()->count();
	}
}
