<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Model;

use PIPEU\Factura\Domain\Abstracts\AbstractDecimal;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class Tax
 *
 * @package PIPEU\Factura\Domain\Model
 * @Flow\Entity
 */
class Tax extends AbstractDecimal {

	/**
	 * {@inheritDoc}
	 */
	const DECIMAL_POINTS = 2;

	/**
	 * {@inheritDoc}
	 */
	public static function getDecimalPoints() {
		return (integer)static::DECIMAL_POINTS;
	}
}
