<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Model\Documents;

use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Factura\Delivery\Domain\Model\InterfaceProvider;
use PIPEU\Factura\Delivery\Domain\Model\ProviderFactory;
use PIPEU\Factura\Domain\Abstracts\AbstractFacturaDocument;
use PIPEU\Factura\Domain\Interfaces\InterfaceCountry;
use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use PIPEU\Factura\Domain\Interfaces\InterfaceTotalWeight;
use PIPEU\Factura\Domain\Interfaces\InterfaceUnit;
use PIPEU\Factura\Domain\Interfaces\InterfaceWeight;
use PIPEU\Factura\Domain\Model\Unit;
use PIPEU\Factura\Domain\Model\Weight;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractPostal;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\Common\Collections\Collection;
use TYPO3\Flow\Property\PropertyMapper;

/**
 * Class Order
 *
 * @package PIPEU\Factura\Domain\Model\Documents
 * @Flow\Entity
 */
class Order extends AbstractFacturaDocument implements InterfaceCountry, InterfaceTotalWeight{

	const TYPE = 'Order';

	/**
	 * @var PropertyMapper
	 * @Flow\Inject
	 * @Flow\Transient
	 */
	protected $propertyMapper;

	/**
	 * @var ProviderFactory
	 * @Flow\Inject
	 * @Flow\Transient
	 */
	protected $deliveryProviderFactory;

	/**
	 * @return ArrayCollection
	 */
	public function getPossibleDeliveries() {
		$possibleDeliveries = new ArrayCollection();
		$possibleProviders = $this->getPossibleDeliveryProviders();
		/** @var InterfaceProvider $provider */
		foreach ($possibleProviders as $provider) {
			$providerProperties = new \ArrayObject();
			$providerProperties->offsetSet('type', (string)$provider->getType());
			$providerProperties->offsetSet('title', (string)$provider->getTitle());
			$providerProperties->offsetSet('subtitle', (string)$provider->getSubtitle());
			$providerProperties->offsetSet('tax', (string)$provider->getTax());
			$providerProperties->offsetSet('unit', (string)1);
			$providerProperties->offsetSet('unitPrice', (string)$provider->getUnitPrice($this));
			$provider = $this->propertyMapper->convert($providerProperties->getArrayCopy(), 'PIPEU\Factura\Domain\Model\Documents\Items\Delivery');
			$possibleDeliveries->add($provider);
		}
		return $possibleDeliveries;
	}

	/**
	 * @return Collection
	 */
	protected function getPossibleDeliveryProviders() {
		$availableProviders = $this->deliveryProviderFactory->getAllProvidersByHighestPriority();
		return $availableProviders->filter($this->getPossibleProviderFilter());
	}

	/**
	 * @return AbstractCountry
	 */
	public function getCountry() {
		$country = NULL;
		if ($this->getSecondaryPostal() instanceof AbstractPostal) {
			if ($this->getSecondaryPostal()->getCountry() instanceof AbstractCountry) {
				$country = $this->getSecondaryPostal()->getCountry();
			} elseif ($this->getPrimaryPostal() instanceof AbstractPostal) {
				if ($this->getPrimaryPostal()->getCountry() instanceof AbstractCountry) {
					$country = $this->getPrimaryPostal()->getCountry();
				}
			}
		}
		return $country;
	}

	/**
	 * @return \Closure
	 */
	protected function getPossibleProviderFilter() {
		return function (InterfaceProvider $provider){
			return $provider->isProvided($this);
		};
	}

	/**
	 * @return Weight
	 */
	public function getTotalWeight() {
		$weight = 0;
		$facturaItemsIterator = $this->getFacturaItems()->getIterator();
		foreach ($facturaItemsIterator as $facturaItem) {
			if ($facturaItem instanceof InterfaceWeight && $facturaItem instanceof InterfaceUnit) {
				$weight += $facturaItem->getUnitWeight()->getValue() * $facturaItem->getUnit()->getValue();
			}
		}
		$weight = (integer)($weight / Unit::getDecimalFactor());
		return new Weight($weight);
	}

	/**
	 * @return boolean
	 */
	public function getIsApproved(){
		return $this->getPrimaryElectronicAddress()->isApproved();
	}
}
