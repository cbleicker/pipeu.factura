<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Model\Documents\Items;

use PIPEU\Factura\Domain\Model\Money;
use PIPEU\Factura\Domain\Model\Tax as DecimalTax;

/**
 * Class Delivery
 *
 * @package PIPEU\Factura\Domain\Model\Documents\Items
 */
class Tax {

	/**
	 * @var Money
	 */
	protected $price;

	/**
	 * @var DecimalTax
	 */
	protected $tax;

	/**
	 * @param Money $price
	 * @param DecimalTax $tax
	 */
	public function __construct(Money $price, DecimalTax $tax) {
		$this->price = $price;
		$this->tax = $tax;
	}

	/**
	 * @return Money
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @return DecimalTax
	 */
	public function getTax() {
		return $this->tax;
	}
}
