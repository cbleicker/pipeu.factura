<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Model\Documents\Items;

use PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem;
use PIPEU\Factura\Domain\Interfaces\InterfaceWeight;
use PIPEU\Factura\Domain\Model\Weight;
use PIPEU\Factura\Domain\Traits\TraitWeight;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Product
 *
 * @package PIPEU\Factura\Domain\Model\Documents\Items
 * @Flow\Entity
 */
class PhysicalProduct extends AbstractFacturaItem implements InterfaceWeight{

	use TraitWeight;

	/**
	 * @var Weight
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $unitWeight;

	/**
	 * @var integer
	 * @ORM\Column(nullable=true)
	 */
	protected $width;

	/**
	 * @var integer
	 * @ORM\Column(nullable=true)
	 */
	protected $length;

	/**
	 * @var integer
	 * @ORM\Column(nullable=true)
	 */
	protected $depth;

	/**
	 * @return Weight
	 */
	public function getTotalWeight() {
		$value = $this->getUnitWeight()->getValue() * $this->getUnit()->getValue() / $this->getUnit()->getDecimalFactor();
		return new Weight((integer)$value);
	}

	/**
	 * @param integer $depth
	 * @return $this
	 */
	public function setDepth($depth = NULL) {
		$this->depth = $depth;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getDepth() {
		return $this->depth;
	}

	/**
	 * @param integer $length
	 * @return $this
	 */
	public function setLength($length = NULL) {
		$this->length = $length;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getLength() {
		return $this->length;
	}

	/**
	 * @param integer $width
	 * @return $this
	 */
	public function setWidth($width = NULL) {
		$this->width = $width;
		return $this;
	}

	/**
	 * @return integer
	 */
	public function getWidth() {
		return $this->width;
	}
}
