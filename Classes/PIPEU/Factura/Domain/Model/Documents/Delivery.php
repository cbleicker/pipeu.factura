<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Model\Documents;

use PIPEU\Factura\Domain\Abstracts\AbstractFacturaDocument;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class Delivery
 *
 * @package PIPEU\Factura\Domain\Model\Documents
 * @Flow\Entity
 */
class Delivery extends AbstractFacturaDocument {

	const TYPE = 'Delivery';
}
