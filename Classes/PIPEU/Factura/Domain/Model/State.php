<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Model;

use PIPEU\Factura\Domain\Abstracts\AbstractState;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class State
 *
 * @package PIPEU\Factura\Domain\Model
 * @Flow\Entity
 */
class State extends AbstractState {

}
