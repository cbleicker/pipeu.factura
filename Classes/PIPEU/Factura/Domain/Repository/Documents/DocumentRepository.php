<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Repository\Documents;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;
use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use TYPO3\Flow\Persistence\QueryResultInterface;

/**
 * Class DocumentRepository
 *
 * @package PIPEU\Factura\Domain\Repository\Documents
 * @Flow\Scope("singleton")
 */
class DocumentRepository extends Repository {

	/**
	 * @var string
	 */
	const ENTITY_CLASSNAME = 'PIPEU\Factura\Domain\Abstracts\AbstractDocument';

	/**
	 * @return QueryResultInterface
	 */
	public function findAllUnarchived() {
		$query = $this->createQuery();
		$query->matching($query->equals('archived', FALSE));
		return $query->execute();
	}

	/**
	 * @return QueryResultInterface
	 */
	public function findAllArchived() {
		$query = $this->createQuery();
		$query->matching($query->equals('archived', TRUE));
		return $query->execute();
	}

	/**
	 * @return QueryResultInterface
	 */
	public function findMainDocuments() {
		$query = $this->createQuery();
		$query->matching(
			$query->equals('parentDocument', NULL)
		);
		return $query->execute();
	}

	/**
	 * @param $serialNumber
	 *
	 * @return AbstractDocument
	 */
	public function findOneBySerialNumber($serialNumber) {
		return $this->findBySerialNumber($serialNumber)->getFirst();
	}

	/**
	 * @param string $serialNumber
	 *
	 * @return QueryResultInterface
	 */
	public function findBySerialNumber($serialNumber) {
		$query = $this->createQuery();
		$query->matching(
			$query->equals('serialNumber', $serialNumber)
		);
		return $query->execute();
	}
}
