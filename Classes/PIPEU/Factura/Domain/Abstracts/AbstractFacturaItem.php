<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Abstracts;

use PIPEU\Factura\Domain\Interfaces\InterfaceDocumentItem;
use PIPEU\Factura\Domain\Model\Money;
use PIPEU\Factura\Domain\Model\Tax;
use PIPEU\Factura\Domain\Model\Unit;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaItem;
use PIPEU\Factura\Domain\Interfaces\InterfaceLabel;
use PIPEU\Factura\Domain\Traits\TraitDocumentItem;
use PIPEU\Factura\Domain\Traits\TraitFacturaItem;
use PIPEU\Factura\Domain\Traits\TraitLabel;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractFacturaItem
 *
 * @package PIPEU\Factura\Domain\Abstracts
 * @Flow\Entity
 */
abstract class AbstractFacturaItem extends AbstractDocumentItem implements InterfaceFacturaItem, InterfaceLabel, InterfaceDocumentItem {

	use TraitFacturaItem;

	/**
	 * @var Money
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $unitPrice;

	/**
	 * @var Tax
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $tax;

	/**
	 * @var Unit
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $unit;

	/**
	 * @var \DateTime
	 * @Flow\Transient
	 */
	protected $availableFrom;
}
