<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Abstracts;

use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaDocument;
use PIPEU\Factura\Domain\Traits\TraitDocument;
use PIPEU\Factura\Domain\Traits\TraitFacturaDocument;
use PIPEU\Geo\Domain\Model\Interfaces\InterfacePostal;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use TYPO3\Flow\I18n\Locale;
use TYPO3\Party\Domain\Model\ElectronicAddress;
use TYPO3\Party\Domain\Model\PersonName;

/**
 * Class AbstractFacturaDocument
 *
 * @package PIPEU\Factura\Domain\Abstracts
 * @Flow\Entity
 */
abstract class AbstractFacturaDocument extends AbstractDocument implements InterfaceFacturaDocument {

	use TraitFacturaDocument;

	/**
	 * @var Collection<AbstractFacturaItem>
	 * @ORM\OneToMany(mappedBy="document")
	 */
	protected $facturaItems;

	/**
	 * @var string
	 * @ORM\Column(nullable=true)
	 */
	protected $vat;

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->facturaItems = new ArrayCollection();
	}

	/**
	 * @return array
	 */
	public function jsonSerialize() {
		$result = parent::jsonSerialize();
		$result['summaryGross'] = $this->getSummaryGross();
		return $result;
	}

	/**
	 * @return float
	 */
	public function getBookingAmount() {
		return (float)(string)$this->getSummaryGross();
	}

	/**
	 * @return InterfacePostal
	 */
	public function getBookingPrimaryPostal() {
		return $this->getPrimaryPostal();
	}

	/**
	 * @return InterfacePostal
	 */
	public function getBookingSecondaryPostal() {
		return $this->getSecondaryPostal();
	}

	/**
	 * @return string
	 */
	public function getTransactionId() {
		return $this->getIdentity();
	}

	/**
	 * @return string
	 */
	public function getUniqueId() {
		return $this->getIdentityHash();
	}

	/**
	 * @return string
	 */
	public function getShortId() {
		return $this->getSerialNumber();
	}

	/**
	 * @return string
	 */
	public function getUsage() {
		return $this->getType() . ' #' . $this->getSerialNumber();
	}

	/**
	 * @return string
	 */
	public function getLanguage() {
		$locale = new Locale($this->getLocaleIdentifier());
		return $locale->getLanguage();
	}

	/**
	 * @return PersonName
	 */
	public function getBookingPrimaryPersonName() {
		return $this->getPrimaryPersonName();
	}

	/**
	 * @return PersonName
	 */
	public function getBookingSecondaryPersonName() {
		return $this->getSecondaryPersonName();
	}

	/**
	 * @return ElectronicAddress
	 */
	public function getBookingPrimaryElectronicAddress() {
		return $this->getPrimaryElectronicAddress();
	}

	/**
	 * @return ElectronicAddress
	 */
	public function getBookingSecondaryElectronicAddress() {
		return $this->getSecondaryElectronicAddress();
	}

	/**
	 * @return string
	 */
	public function getBookingPrimaryCompany() {
		return $this->getPrimaryCompany();
	}

	/**
	 * @return string
	 */
	public function getBookingSecondaryCompany() {
		return $this->getSecondaryCompany();
	}
}
