<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Abstracts;

use PIPEU\Factura\Domain\Interfaces\InterfaceDecimal;
use PIPEU\Factura\Domain\Traits\TraitDecimal;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractDecimal
 *
 * @package PIPEU\Site\Domain\Model
 */
abstract class AbstractDecimal implements InterfaceDecimal {

	use TraitDecimal;

	/**
	 * The DecimalFactor Constant to "draw" decimalpoints in string repreasantions
	 * Also used for calculations f.e. round($foo / DECIMAL_POINTS, DECIMAL_POINTS)
	 */
	const DECIMAL_POINTS = 2;

	/**
	 * @var integer
	 */
	protected $value;

	/**
	 * @param integer $value
	 */
	public function __construct($value = 0) {
		$this->value = (integer)$value;
	}
}
