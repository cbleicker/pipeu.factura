<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Abstracts;

use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use TYPO3\Party\Domain\Model\ElectronicAddress;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractPostal;
use PIPEU\Factura\Context\InterfaceFactura as FacturaContext;
use PIPEU\Factura\Domain\Interfaces\InterfaceDocument;
use PIPEU\Factura\Domain\Traits\TraitDocument;
use Doctrine\Common\Collections\Collection;
use TYPO3\Party\Domain\Model\PersonName;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;

/**
 * Class AbstractDocument
 *
 * @package PIPEU\Factura\Domain\Abstracts
 * @Flow\Entity
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractDocument implements InterfaceDocument {

	use TraitDocument;

	/**
	 * @var PersistenceManagerInterface
	 * @Flow\Inject
	 * @Flow\Transient
	 */
	protected $persistenceManager;

	/**
	 * @var Collection<AbstractState>
	 * @ORM\ManyToMany(cascade={"all"}, orphanRemoval=true)
	 * @ORM\OrderBy({"dateTime" = "DESC"})
	 */
	protected $states;

	/**
	 * @var AbstractState
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $primaryState;

	/**
	 * @var AbstractDocument
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $parentDocument;

	/**
	 * @var Collection<AbstractDocument>
	 * @Flow\Lazy
	 * @ORM\OneToMany(mappedBy="parentDocument")
	 * @ORM\OrderBy({"dateTime" = "ASC"})
	 */
	protected $childDocuments;

	/**
	 * @var string
	 * @ORM\Column(nullable=true)
	 */
	protected $localeIdentifier;

	/**
	 * @var integer
	 * @ORM\Column(unique=true, type="bigint", nullable=false, columnDefinition="BIGINT AUTO_INCREMENT")
	 */
	protected $serialNumber;

	/**
	 * @var \DateTime
	 */
	protected $dateTime;

	/**
	 * @var FacturaContext
	 * @Flow\Inject
	 * @Flow\Transient
	 */
	protected $context;

	/**
	 * @var AbstractPostal
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $primaryPostal;

	/**
	 * @var AbstractPostal
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $secondaryPostal;

	/**
	 * @var PersonName
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $primaryPersonName;

	/**
	 * @var PersonName
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $secondaryPersonName;

	/**
	 * @var ElectronicAddress
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $primaryElectronicAddress;

	/**
	 * @var ElectronicAddress
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $secondaryElectronicAddress;

	/**
	 * @var string
	 * @ORM\Column(nullable=true)
	 */
	protected $primaryCompany;

	/**
	 * @var string
	 * @ORM\Column(nullable=true)
	 */
	protected $secondaryCompany;

	/**
	 * @var boolean
	 */
	protected $archived;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->dateTime = new \DateTime();
		$this->childDocuments = new ArrayCollection();
		$this->states = new ArrayCollection();
		$this->archived = FALSE;
	}

	/**
	 * @return array
	 */
	public function jsonSerialize() {
		return array(
			'identity' => $this->getIdentity(),
			'dateTime' => $this->getDateTime(),
			'type' => $this->getType(),
			'serialNumber' => $this->getSerialNumber(),
			'primaryPersonName' => $this->getPrimaryPersonName(),
			'secondaryPersonName' => $this->getSecondaryPersonName(),
			'primaryElectronicAddress' => $this->getPrimaryElectronicAddress(),
			'secondaryElectronicAddress' => $this->getSecondaryElectronicAddress(),
			'primaryPostal' => $this->getPrimaryPostal(),
			'secondaryPostal' => $this->getSecondaryPostal(),
			'states' => $this->getStates(),
			'primaryState' => $this->getPrimaryState(),
			'archived' => $this->getArchived()
		);
	}
}
