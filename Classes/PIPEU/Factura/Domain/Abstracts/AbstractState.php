<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Abstracts;

use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use PIPEU\Factura\Domain\Model\State;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractState
 *
 * @package PIPEU\Factura\Domain\Abstracts
 * @Flow\Entity
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractState implements InterfaceState {

	/**
	 * The error message, could also be a key for translation.
	 *
	 * @var string
	 * @ORM\Column(nullable=true)
	 */
	protected $message;

	/**
	 * An optional title for the message (used eg. in flashMessages).
	 *
	 * @var string
	 * @ORM\Column(nullable=true)
	 */
	protected $title;

	/**
	 * The error code.
	 *
	 * @var integer
	 */
	protected $code;

	/**
	 * The message arguments. Will be replaced in the message body.
	 *
	 * @var array
	 */
	protected $arguments = array();

	/**
	 * The severity of this message ('OK'), overwrite in your own implementation.
	 *
	 * @var string
	 */
	protected $severity;

	/**
	 * @var \DateTime
	 */
	protected $dateTime;

	/**
	 * Constructs this state
	 *
	 * @param integer $code
	 * @param string $severity
	 * @param string $message
	 * @param string $title
	 * @param array $arguments
	 */
	public function __construct($code, $severity = InterfaceState::SEVERITY_OK, $message = NULL, $title = NULL, array $arguments = array()) {
		$this->code = $code;
		$this->message = $message;
		$this->title = $title;
		$this->arguments = $arguments;
		$this->severity = $severity;
		$this->dateTime = new \DateTime();
	}

	/**
	 * Returns the error message
	 *
	 * @return string The error message
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * Returns the error code
	 *
	 * @return integer The error code
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @return array
	 */
	public function getArguments() {
		return $this->arguments;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getSeverity() {
		return $this->severity;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateTime() {
		return $this->dateTime;
	}

	/**
	 * @return string
	 */
	public function render() {
		if ($this->arguments !== array()) {
			return vsprintf($this->message, $this->arguments);
		} else {
			return $this->message;
		}
	}

	/**
	 * @param integer $code
	 *
	 * @return \Closure
	 */
	public static function filterStateByCode($code) {
		return function (State $state) use ($code) {
			return $state->getCode() === $code;
		};
	}

	/**
	 * Converts this error into a string
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->render();
	}
}
