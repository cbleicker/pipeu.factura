<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Abstracts;

use PIPEU\Factura\Domain\Interfaces\InterfaceDocumentItem;
use PIPEU\Factura\Domain\Interfaces\InterfaceLabel;
use PIPEU\Factura\Domain\Traits\TraitDocumentItem;
use PIPEU\Factura\Domain\Traits\TraitLabel;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractDocumentItem
 *
 * @package PIPEU\Factura\Domain\Abstracts
 * @Flow\Entity
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractDocumentItem implements InterfaceDocumentItem, InterfaceLabel {

	use TraitDocumentItem, TraitLabel;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var string
	 */
	protected $subtitle;

	/**
	 * @var AbstractDocument
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	protected $document;

}
