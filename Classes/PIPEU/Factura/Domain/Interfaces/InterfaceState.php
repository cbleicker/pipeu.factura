<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Interfaces;

/**
 * Class InterfaceState
 *
 * @package PIPEU\Factura\Domain\Interfaces
 */
interface InterfaceState {

	const CODE_PAID = 1401712161,
		CODE_APPROVED = 1401712162,
		CODE_REVERSED = 1401712163,
		CODE_REFUNDED = 1401712164,
		CODE_SHIPPED = 1401712165,
		CODE_REMINDED = 1413642680,
		CODE_APPOINTED = 141469509,
		CODE_CAPTURE = 1414695091,
		CODE_UNDERPAID = 1414695092,
		CODE_CANCELATION = 1414695093,
		CODE_REFUND = 1414695094,
		CODE_DEBIT = 1414695095,
		CODE_REMINDER = 1414695096,
		CODE_VAUTHORIZATION = 1414695097,
		CODE_VSETTLEMENT = 1414695098,
		CODE_TRANSFER = 1414695099,
		CODE_INVOICE = 1414695100;

	const SEVERITY_NOTICE = 'Notice';

	const SEVERITY_WARNING = 'Warning';

	const SEVERITY_ERROR = 'Error';

	const SEVERITY_OK = 'OK';

	/**
	 * Returns the error message
	 *
	 * @return string The error message
	 */
	public function getMessage();

	/**
	 * Returns the error code
	 *
	 * @return integer The error code
	 */
	public function getCode();

	/**
	 * @return array
	 */
	public function getArguments();

	/**
	 * @return string
	 */
	public function getTitle();

	/**
	 * @return string
	 */
	public function getSeverity();

	/**
	 * @return \DateTime
	 */
	public function getDateTime();

	/**
	 * @return string
	 */
	public function render();

	/**
	 * Converts this error into a string
	 *
	 * @return string
	 */
	public function __toString();

	/**
	 * @param integer $identity
	 * @return \Closure
	 */
	public static function filterStateByCode($identity);
}
