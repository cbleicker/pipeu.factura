<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Interfaces;

use PIPEU\Factura\Domain\Model\Money;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Factura\Domain\Abstracts\AbstractFacturaItem;
use PIPEU\Factura\Domain\Model\Documents\Items\Tax;
use PIPEU\Factura\Domain\Interfaces\InterfaceState;
use PIPEU\Payment\Domain\Model\Interfaces\PaymentDataInterface;

/**
 * Class InterfaceFacturaDocument
 *
 * @package PIPEU\Factura\Domain\Interfaces
 */
interface InterfaceFacturaDocument extends InterfaceDocument, PaymentDataInterface {

	/**
	 * @return Collection<AbstractFacturaItem>
	 */
	public function getFacturaItems();

	/**
	 * @param AbstractFacturaItem $facturaItem
	 * @return $this
	 */
	public function addFacturaItem(AbstractFacturaItem $facturaItem);

	/**
	 * @param AbstractFacturaItem $facturaItem
	 * @return $this
	 */
	public function removeFacturaItem(AbstractFacturaItem $facturaItem);

	/**
	 * @return $this
	 */
	public function pruneFacturaItems();

	/**
	 * @param Collection $facturaItems
	 * @return $this
	 */
	public function setFacturaItems(Collection $facturaItems);

	/**
	 * @return string
	 */
	public function getVat();

	/**
	 * @param string $vat
	 * @return $this
	 */
	public function setVat($vat = NULL);

	/**
	 * @return boolean
	 */
	public function getIsTaxable();

	/**
	 * @return Money
	 */
	public function getSummary();

	/**
	 * @return Money
	 */
	public function getSummaryGross();

	/**
	 * @return ArrayCollection<Tax>
	 */
	public function getTaxes();

	/**
	 * @return Collection<InterfaceState>
	 */
	public function getPaidStates();

	/**
	 * return boolean
	 */
	public function getIsPaid();
}
