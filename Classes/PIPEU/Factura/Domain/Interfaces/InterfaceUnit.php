<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Interfaces;

use PIPEU\Factura\Domain\Model\Unit;

/**
 * Interface InterfaceUnit
 *
 * @package PIPEU\Factura\Domain\Interfaces
 */
interface InterfaceUnit {

	/**
	 * @return Unit
	 */
	public function getUnit();

	/**
	 * @param Unit $unit
	 * @return $this
	 */
	public function setUnit(Unit $unit);

}
