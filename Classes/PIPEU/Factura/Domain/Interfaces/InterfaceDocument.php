<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Interfaces;

use PIPEU\Factura\Domain\Abstracts\AbstractState;
use TYPO3\Party\Domain\Model\ElectronicAddress;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractPostal;
use PIPEU\Factura\Context\InterfaceFactura as FacturaContext;
use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use Doctrine\Common\Collections\Collection;
use TYPO3\Party\Domain\Model\PersonName;
use TYPO3\Flow\Exception;

/**
 * Class InterfaceDocument
 *
 * @package PIPEU\Factura\Domain\Interfaces
 */
interface InterfaceDocument extends \JsonSerializable{

	const TYPE = 'Undefined';

	/**
	 * @return string
	 */
	public function getType();

	/**
	 * @param AbstractDocument $parentDocument
	 *
	 * @return $this;
	 */
	public function setParentDocument(AbstractDocument $parentDocument);

	/**
	 * @return AbstractDocument
	 */
	public function getParentDocument();

	/**
	 * @return Collection<AbstractDocument>
	 */
	public function getChildDocuments();

	/**
	 * @return AbstractDocument
	 */
	public function getMainDocument();

	/**
	 * @return string
	 */
	public function getLocaleIdentifier();

	/**
	 * @return integer
	 */
	public function getSerialNumber();

	/**
	 * @param \DateTime $dateTime
	 *
	 * @return $this
	 */
	public function setDateTime(\DateTime $dateTime);

	/**
	 * @return \DateTime
	 */
	public function getDateTime();

	/**
	 * @return FacturaContext
	 */
	public function getContext();

	/**
	 * @param AbstractPostal $primaryPostal
	 *
	 * @return $this
	 */
	public function setPrimaryPostal(AbstractPostal $primaryPostal);

	/**
	 * @return AbstractPostal
	 */
	public function getPrimaryPostal();

	/**
	 * @param AbstractPostal $secondaryPostal
	 *
	 * @return $this
	 */
	public function setSecondaryPostal(AbstractPostal $secondaryPostal);

	/**
	 * @return AbstractPostal
	 */
	public function getSecondaryPostal();

	/**
	 * @param ElectronicAddress $primaryElectronicAddress
	 *
	 * @return $this
	 */
	public function setPrimaryElectronicAddress(ElectronicAddress $primaryElectronicAddress);

	/**
	 * @return ElectronicAddress
	 */
	public function getPrimaryElectronicAddress();

	/**
	 * @param ElectronicAddress $secondaryElectronicAddress
	 *
	 * @return $this
	 */
	public function setSecondaryElectronicAddress(ElectronicAddress $secondaryElectronicAddress);

	/**
	 * @return ElectronicAddress
	 */
	public function getSecondaryElectronicAddress();

	/**
	 * @param AbstractState $state
	 *
	 * @return $this
	 */
	public function addState(AbstractState $state);

	/**
	 * @param AbstractState $state
	 *
	 * @return $this
	 */
	public function removeState(AbstractState $state);

	/**
	 * @param Collection<AbstractState> $states
	 *
	 * @return $this
	 */
	public function setStates(Collection $states);

	/**
	 * @return Collection<AbstractState>
	 */
	public function getStates();

	/**
	 * @param AbstractState $state
	 *
	 * @return $this
	 */
	public function setPrimaryState(AbstractState $state);

	/**
	 * @return AbstractState
	 */
	public function getPrimaryState();

	/**
	 * @return boolean
	 */
	public function getIsReversed();

	/**
	 * @param PersonName $personName
	 *
	 * @return $this
	 */
	public function setPrimaryPersonName(PersonName $personName);

	/**
	 * @return PersonName
	 */
	public function getPrimaryPersonName();

	/**
	 * @param PersonName $personName
	 *
	 * @return $this
	 */
	public function setSecondaryPersonName(PersonName $personName);

	/**
	 * @return PersonName
	 */
	public function getSecondaryPersonName();

	/**
	 * @return string
	 */
	public function getPrimaryCompany();

	/**
	 * @return string
	 */
	public function getSecondaryCompany();

	/**
	 * @return string
	 */
	public function getIdentityHash();

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getAuthenticationHash();

	/**
	 * @return string
	 */
	public function getIdentity();

	/**
	 * @param boolean $archived
	 * @return $this
	 */
	public function setArchived($archived = TRUE);

	/**
	 * @return boolean
	 */
	public function getArchived();
}
