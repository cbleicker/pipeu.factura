<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Interfaces;

use PIPEU\Factura\Domain\Abstracts\AbstractDocument;
use PIPEU\Site\Domain\Model\Money;
use PIPEU\Site\Domain\Model\Tax;
use PIPEU\Site\Domain\Model\Unit;

/**
 * Interface InterfaceFacturaItem
 *
 * @package PIPEU\Factura\Domain\Interfaces
 */
interface InterfaceDocumentItem {

	/**
	 * @return AbstractDocument
	 */
	public function getDocument();

	/**
	 * @param AbstractDocument $document
	 * @return $this
	 */
	public function setDocument(AbstractDocument $document);

}
