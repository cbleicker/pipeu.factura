<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Interfaces;

use PIPEU\Factura\Domain\Model\Tax;

/**
 * Class InterfaceTax
 *
 * @package PIPEU\Factura\Domain\Interfaces
 */
interface InterfaceTax {

	/**
	 * @return Tax
	 */
	public function getTax();

	/**
	 * @param Tax $tax
	 * @return $this
	 */
	public function setTax(Tax $tax);

}
