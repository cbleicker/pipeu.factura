<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Interfaces;

use PIPEU\Geo\Domain\Model\Country;

/**
 * Class InterfaceCountry
 *
 * @package PIPEU\Factura\Domain\Interfaces
 */
interface InterfaceCountry {

	/**
	 * @return Country
	 */
	public function getCountry();
}
