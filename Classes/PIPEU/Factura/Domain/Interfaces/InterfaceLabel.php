<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Domain\Interfaces;

/**
 * Interface InterfaceLabel
 *
 * @package PIPEU\Factura\Domain\Interfaces
 */
interface InterfaceLabel {

	/**
	 * @return string
	 */
	public function getTitle();

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title);

	/**
	 * @return string
	 */
	public function getSubtitle();

	/**
	 * @param string $subtitle
	 * @return $this
	 */
	public function setSubtitle($subtitle);
}
